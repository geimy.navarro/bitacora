package co.com.pragma.certificacion.questions;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;


import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.ALERTA_TEXT;



public class ElMensajeDeLaAlerta implements Question <String>{


    @Override
    public  String answeredBy(Actor actor) {
        //actor.attemptsTo(Wait.theSeconds(1));
        return ALERTA_TEXT.resolveFor(actor).getText();
    }

    public static Question<String> text(){
        return new ElMensajeDeLaAlerta();
    }


}
