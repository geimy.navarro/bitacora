package co.com.pragma.certificacion.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.pragma.certificacion.userinterfaces.InicioPage.INICIO_CONTAINER;


public class LaPaginaDeInicio implements Question <Boolean>{


    @Override
    public Boolean answeredBy(Actor actor) {
        return INICIO_CONTAINER.resolveFor(actor).isVisible();
    }

    public static Question<Boolean> visible(){
        return new LaPaginaDeInicio();
    }


}
