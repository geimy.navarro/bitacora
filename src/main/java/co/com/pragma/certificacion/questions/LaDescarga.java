package co.com.pragma.certificacion.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.io.File;

import static co.com.pragma.certificacion.userinterfaces.InicioPage.INICIO_CONTAINER;


public class LaDescarga implements Question<Boolean> {

    private String archivo;

    public LaDescarga(String archivo) {
        this.archivo = archivo;
    }

    @Override
    public Boolean answeredBy(Actor actor) {

        File archivoDescarga = new File(archivo);
       return archivoDescarga.exists();
    }

    public static Question<Boolean> esExitosa(String archivo) {
        return new LaDescarga(archivo);
    }


}
