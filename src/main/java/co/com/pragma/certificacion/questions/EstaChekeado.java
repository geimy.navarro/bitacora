package co.com.pragma.certificacion.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.CHECKEADO;


public class EstaChekeado implements Question<Boolean> {


    @Override
    public Boolean answeredBy(Actor actor) {
        boolean visa = CHECKEADO("false", "1").resolveFor(actor).isVisible();
        boolean trabajarEstadosUnidos = CHECKEADO("false","2").resolveFor(actor).isVisible();

        if (visa && trabajarEstadosUnidos) {
            return true;
        }
        return false;
    }

    public static Question<Boolean> visible() {
        return new EstaChekeado();
    }


}
