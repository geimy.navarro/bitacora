package co.com.pragma.certificacion.userinterfaces.pragmadata;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

@DefaultUrl("https://d3jvm6ee97l28q.cloudfront.net/login")
public class InicioPragmaData extends PageObject {

    public static final Target INICIO_CONTAINER = Target.the("Pagina de Inicio").
            located(By.id("menuDropContainer"));

    public static final Target PRAGMA_DATA_OPTION = Target.the("PragmaData").
            located(By.xpath("//a[@href='/pragmadata']"));


    public static Target OPCION_PRAGMA_DATA_CHECKBOX(String opcionPragmaData) {
        return Target.the("Conocimientos tecnicos").
                located(By.xpath(" //div[2]/div[" + opcionPragmaData + "]/input[@name='data.campoTabla']"));

    }
    public static Target OTRA_HOJA_CHECKBOX(String opcionPragmaData) {
        return Target.the("Conocimientos tecnicos").
                located(By.xpath(" //div[3]/div[" + opcionPragmaData + "]/input[@name='data.campoTabla']"));

    }


    public static final Target AGREGAR_FILTRO_BUTTON = Target.the("Agregar filtro").
            located(By.xpath("//button[@class='pragmaData__buttons']"));

    public static final Target EXPORTAR_BUTTON = Target.the("Boton exporta").
            located(By.id("exportButton"));


}





