package co.com.pragma.certificacion.userinterfaces.perfil;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

@DefaultUrl("https://d3jvm6ee97l28q.cloudfront.net/login")
public class InformacionPerfilPage extends PageObject {


    // Descargar la hoja de vida en otro idioma

    public static final Target INFORMACION_LABORAL_TAB = Target.the("Informacion laboral").
            located(By.cssSelector("[routerlink='../main/experiencia'] > .tab-title"));

    public static final Target INFORMACION_ACADEMICA_TAB = Target.the("Informacion Academica").
            located(By.cssSelector("[routerlink='../main/informacion-academica'] > .tab-title"));

    public static final Target DESCARGAR_HOJA_DE_VIDA_BUTTON = Target.the("Hoja de vida").
            located(By.xpath("//div[@class='profile-name']//a[contains(.,'Hoja de vida')]"));

    public static final Target IDIOMA_HOJA_DE_VIDA_BUTTON = Target.the("Idioma hoja de vida").
            located(By.cssSelector(".a-selectBasic__select"));

    public static final Target GENERAR_HOJA_DE_VIDA_BUTTON = Target.the("Generar hoja de vida").
            located(By.cssSelector(".filled"));

}

