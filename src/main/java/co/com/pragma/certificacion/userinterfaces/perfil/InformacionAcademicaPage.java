package co.com.pragma.certificacion.userinterfaces.perfil;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

@DefaultUrl("https://d3jvm6ee97l28q.cloudfront.net/login")
public class InformacionAcademicaPage extends PageObject {


    //se diligenia la informacion academica de estudios


    public static final Target AGREGAR_EDUCACION_BUTTON = Target.the("Experiencia laboral pragma").
            located(By.xpath("//div[@class='education-info-header']/button[@class='add-btn']"));

    public static final Target TIPO_NIVEL_COMBOBOX = Target.the("Tipo/nivel").
            located(By.cssSelector(".education-info-form[novalidate] .mat-select-placeholder"));

    public static Target TIPO_NIVEL(String tipoNivel) {
        return Target.the("TIPO/NIVEL").
                located(By.cssSelector("[ng-reflect-value='" + tipoNivel + "']"));
    }

    public static final Target TITULO_OBTENIDO_INPUT = Target.the("Titulo").
            located(By.cssSelector(".education-info-form[novalidate] [name='title']"));

    public static final Target ANIO_DE_FINALIZACION_INPUT = Target.the("Anio de finalizacion").
            located(By.cssSelector(".education-info-form[novalidate] [type='number']"));

    public static final Target INSTITUCION_INPUT = Target.the("Institucion").
            located(By.cssSelector(".education-info-form[novalidate] [name='institute']"));


// Se diligancia la informacion de otros estudios

    public static final Target AGREGAR_OTROS_ESTUDIOS_BUTTON = Target.the("Agregar otros estudios").
            located(By.xpath("//div[@class='certificate-header']/button[@class='add-btn']"));

    public static final Target TITULO_DE_LA_CERTICACION_INPUT = Target.the("Titulo").
            located(By.cssSelector(".certificate-form[novalidate] [name='Title']"));

    public static final Target EMPRESA_DICTA_INPUT = Target.the("Empresa").
            located(By.cssSelector(".certificate-form[novalidate] [name='Institute']"));

    public static final Target ANIO_DE_FINALIZACION_OTROS_INPUT = Target.the("Anio de finalizacion").
            located(By.cssSelector(".certificate-form[novalidate] [name='Year']"));

    public static final Target VIGENCIA_CERTIFICADO_INPUT = Target.the("Vigencia").
            located(By.cssSelector(".certificate-form[novalidate] [name='ExpirationDate']"));

    public static final Target DURACION_INPUT = Target.the("Duracion").
            located(By.cssSelector(".certificate-form[novalidate] [name='Hours']"));


    public static final Target PALABRAS_CLAVES_COMBOBOX = Target.the("Palabras claves").
            located(By.cssSelector("[ng-reflect-autocomplete='[object Object]']"));

    public static Target PALABRAS_CLAVES(String palabra) {
        return Target.the("Palabras claves").
                located(By.cssSelector("[ng-reflect-value='" + palabra + "']"));
    }

    public static final Target CERRAR_PANEL_BUTTON = Target.the("Adjuntar certificado").
            located(By.className("overlay-pane-close"));


    public static final Target ADJUNTAR_CERTIFICADO_BUTTON = Target.the("Adjuntar certificado").
            located(By.cssSelector(".field"));

    public static final Target ALERTA_TEXT = Target.the("Mensaje de guardaoo exitoso").
            located(By.cssSelector(".mat-simple-snackbar > span"));

}
