package co.com.pragma.certificacion.userinterfaces.perfil;

import groovyjarjarpicocli.CommandLine;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

@DefaultUrl("https://d3jvm6ee97l28q.cloudfront.net/login")
public class InformacionLaboralPage extends PageObject {

    // Se diligancia la informacion de la hoja de vida


    public static final Target PROFESION_COMBOBOX = Target.the("Informacion laboral").
            located(By.id("profession"));

    public static Target PROFESIONES_LISTAS(String profesiones) {
        return Target.the("Lista de profesiones").
                located(By.cssSelector("[ng-reflect-value='" + profesiones + "']"));
    }

    public static final Target PERFIL_PROFESIONAL_INPUT = Target.the("Perfil profesiona").
            located(By.id("summary"));

    public static final Target CONOCIMIENTOS_TECNICOS_INPUT = Target.the("Conocimientos tecnicos").
            located(By.id("skillsDescription"));

    public static final Target CARGO_INPUT = Target.the("Cargo").
            located(By.cssSelector("[formcontrolname='userRole']"));

    public static final Target VICEPRESIDENCIA_COMBOBOX = Target.the("Vicepresidencia").
            located(By.id("vicePresidency"));

    public static final Target VICEPRESIDENCIA_LISTA(String vicepresidencia) {
        return Target.the("Vicepresidencia").
                located(By.xpath("//span[@class='mat-option-text' and contains(.,'" +
                        vicepresidencia + "')]"));
    }

    public static final Target ANIOS_EXPERIENCIA_INPUT = Target.the("Experiencia").
            located(By.cssSelector("[formcontrolname='yearExperience']"));

    public static final Target TENER_VISA_CHECK = Target.the("Tener visa").
            located(By.cssSelector("[for='mat-checkbox-1-input']"));

    public static final Target TRABAJAR_EEUU_CHECK = Target.the("Trabajar EEUU").
            located(By.cssSelector("[for='mat-checkbox-2-input']"));

    public static final Target CHECKEADO(String estado, String check) {
        return Target.the("Estado").
                located(By.xpath("//input[@id='mat-checkbox-" + check + "-input' and @aria-checked='" + estado + "']"));
    }

    public static final Target GUARDAR_BUTTON = Target.the("Boton guardar").
            located(By.xpath(".//button[@class='btn save-btn ng-star-inserted']"));

    public static final Target SWITCH_BUTTON = Target.the("Switch idioma").
            located(By.cssSelector(".mat-slide-toggle-bar"));


    // Se diligancia la informacion de experiencia laborar en pragma

    public static final Target AGREGAR_EXPERIENCIA_PRAGMA_BUTTON = Target.the("Experiencia laboral pragma").
            located(By.id("addInt"));

    public static final Target PROYECTO_PRAGMA_INPUT = Target.the("Proyecto pragma ").
            located(By.cssSelector("[formcontrolname='project']"));

    public static final Target FECHA_INGRESO_INPUT = Target.the("Fecha ingreso").
            located(By.cssSelector("form:nth-of-type(2) [formcontrolname='startDate']"));

    public static final Target FECHA_FINALIZACION_INPUT = Target.the("Fecha finalizacion").
            located(By.cssSelector("form:nth-of-type(2) [formcontrolname='endDate']"));

    public static final Target LOGROS_INPUT = Target.the("Logros").
            located(By.cssSelector("form:nth-of-type(2) [formcontrolname='tasks']"));

    public static final Target HERRAMIENTAS_INPUT = Target.the("Herramientas").
            located(By.cssSelector("form:nth-of-type(2) [formcontrolname='usedTools']"));

    public static final Target DESCRIPCION_INPUT = Target.the("Descripcion").
            located(By.cssSelector("form:nth-of-type(2) [formcontrolname='description']"));


    public static final Target DESCARTAR_BUTTON = Target.the("Boton descartar").
            located(By.xpath("//button[@class='btn discard-btn']"));

    public static final Target GUARDAR_EXPERIENCIAS_BUTTON = Target.the("Boton guardar").
            located(By.xpath("//button[@class='btn save-btn']"));


// Se diligancia la informacion de experiencia laborar externa

    public static final Target AGREGAR_EXPERIENCIA_EXTERNA_BUTTON = Target.the("Experiencia laboral pragma").
            located(By.id("addExt"));

    public static final Target EMPRESA_INPUT = Target.the("Empresa").
            located(By.cssSelector("[formcontrolname='company']"));

    public static final Target ROL_INPUT = Target.the("Rol Pragma ").
            located(By.cssSelector("form:nth-of-type(2) [formcontrolname='role']"));

    public static final Target ALERTA_TEXT = Target.the("Mensaje de guardaoo exitoso").
            located(By.cssSelector(".mat-simple-snackbar > span"));


}
