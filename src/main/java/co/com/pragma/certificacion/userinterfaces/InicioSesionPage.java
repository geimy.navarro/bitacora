package co.com.pragma.certificacion.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;


public class InicioSesionPage extends PageObject {

    public static Target INICIAR_SESION_CON_GOOGLE_BOTTON = Target.the("Iniciar Sesion con Google").
            located(By.id("customBtn"));
    public static Target CORREO_INPUT = Target.the("Correo").
            located(By.id("identifierId"));
    public static Target SIGUIENTE_BUTTON = Target.the("Siguiente").
            locatedBy("//span[.='Siguiente']");

    public static Target CONTRASENIA_INPUT = Target.the("Contraseña").
            located(By.name("password"));

    public static Target PROBAR_OTRA_MANERA_BUTTON = Target.the("Probra otra manera").
            locatedBy("//span[.='Probar otra manera']");

    public static Target USAR_AUTEN_BUTTON = Target.the("Aplicación Authenticator").
            locatedBy("[data-challengeindex='2']");


    public static Target OPT_INPUT = Target.the("").
            located(By.id("totpPin"));


}

