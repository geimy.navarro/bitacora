package co.com.pragma.certificacion.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

@DefaultUrl("https://d3jvm6ee97l28q.cloudfront.net/login")
public class InicioPage extends PageObject {

    public static final Target INICIO_CONTAINER = Target.the("Pagina de Inicio").
            located(By.id("menuDropContainer"));
    public static final Target FOTO_PERFIL_DIV = Target.the("Foto Perfil").
            located(By.cssSelector(".pic-profile"));
    public static final Target EDITAR_PERFIL_OPTION = Target.the("Editar Perfil").
            located(By.xpath("//span[.='Editar perfil']"));


}





