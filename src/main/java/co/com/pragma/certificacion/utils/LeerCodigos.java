package co.com.pragma.certificacion.utils;


import org.jboss.aerogear.security.otp.Totp;


public class LeerCodigos {

    public static String getTwoFactorCode() {
        //Codigo correo de pruebas
        //Totp totp = new Totp("t3uo swuv ulfz fue6 mdve av7i i47w magk");
        //Codigo de pruebas personal
      Totp totp = new Totp("g35u qvnk pvyy zy64 rwa6 5kow v55w sjk6");
        String twoFactorCode = totp.now();
        return twoFactorCode;
    }
}