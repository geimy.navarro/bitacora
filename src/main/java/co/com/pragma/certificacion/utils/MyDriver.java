package co.com.pragma.certificacion.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class MyDriver {
    private static WebDriver driver;

    public static MyDriver aChromeDriver() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
       options.addArguments("--start-maximized", "--disable-infobars");
        driver = new ChromeDriver(options);
        return new MyDriver();
    }
    public static MyDriver aFirefox() {
        System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe");
        FirefoxOptions options = new FirefoxOptions();
       // options.addArguments("--incognito", "--start-maximized", "--disable-infobars");
        driver = new FirefoxDriver(options);
        return new MyDriver();
    }
    public WebDriver inTheWebPage(String url) {
        driver.get(url);
        return driver;
    }
}