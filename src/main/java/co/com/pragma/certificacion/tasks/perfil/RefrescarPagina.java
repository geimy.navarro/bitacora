package co.com.pragma.certificacion.tasks.perfil;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.openqa.selenium.WebDriver;



//En esta clase se realiza el refrescar la pagina

public class RefrescarPagina implements Task {


    @Override
    public <T extends Actor> void performAs(T actor) {
        WebDriver driver = BrowseTheWeb.as(actor).getDriver();
        driver.navigate().refresh();

    }


    public static  RefrescarPagina delNavegador() {
        return Tasks.instrumented(RefrescarPagina.class);

    }


}
