package co.com.pragma.certificacion.tasks.perfil;


import co.com.pragma.certificacion.interations.Wait;
import co.com.pragma.certificacion.model.Educacion;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;


import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionAcademicaPage.*;
import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.GUARDAR_BUTTON;


public class DiligenciarEducacion implements Task {

    private Educacion educacion;

    public DiligenciarEducacion(Educacion educacion) {
        this.educacion = educacion;
    }

    // Paso a paso de ingreso a informacion academica
    @Override
    public <T extends Actor> void performAs(T actor) {

        // Se diligencia la informacion academica  educacion para estudios
        actor.attemptsTo(Click.on(AGREGAR_EDUCACION_BUTTON));
        actor.attemptsTo(Click.on(TIPO_NIVEL_COMBOBOX), Wait.theSeconds(3));
        actor.attemptsTo(Click.on(TIPO_NIVEL(educacion.getTipoNivel())));
        actor.attemptsTo(Enter.theValue(educacion.getTituloObtenido()).into(TITULO_OBTENIDO_INPUT));
        actor.attemptsTo(Enter.theValue(educacion.getAnioFinalizacion()).into(ANIO_DE_FINALIZACION_INPUT));
        actor.attemptsTo(Enter.theValue(educacion.getInstitucion()).into(INSTITUCION_INPUT));
        actor.attemptsTo(Click.on(GUARDAR_BUTTON), Wait.theSeconds(5));
        actor.attemptsTo(Click.on(CERRAR_PANEL_BUTTON), Wait.theSeconds(5));


    }

    // Este metodo nos llama el usuario desde el step
    public static DiligenciarEducacion con(Educacion educacion) {
        return Tasks.instrumented(DiligenciarEducacion.class, educacion);
    }

}










