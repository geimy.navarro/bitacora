package co.com.pragma.certificacion.tasks;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.pragma.certificacion.userinterfaces.pragmadata.InicioPragmaData.PRAGMA_DATA_OPTION;

public class OpcionesDeInicio {

    public static Performable irPragmaData(){
      //  return Task.where(actor -> {actor.attemptsTo(Click.on(PRAGMA_DATA_OPTION));});
        return Click.on(PRAGMA_DATA_OPTION);
    }
}
