package co.com.pragma.certificacion.tasks.perfil;

import co.com.pragma.certificacion.interations.Wait;
import co.com.pragma.certificacion.model.InformacionHojaDeVida;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import net.serenitybdd.screenplay.actions.Enter;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.*;


public class DiligenciarInformacionHojaDeVidaES implements Task {

    private InformacionHojaDeVida infoLaboral;
    public DiligenciarInformacionHojaDeVidaES(InformacionHojaDeVida infoLaboral) {
        this.infoLaboral = infoLaboral;
    }

    // Paso a paso de ingreso
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(PROFESION_COMBOBOX));
        actor.attemptsTo(Click.on(PROFESIONES_LISTAS (infoLaboral.getProfesion())),Wait.theSeconds(5));
        actor.attemptsTo(Enter.theValue(infoLaboral.getPrefilProfesional()).into(PERFIL_PROFESIONAL_INPUT));
        actor.attemptsTo(Enter.theValue(infoLaboral.getConocimientosTecnicos()).into(CONOCIMIENTOS_TECNICOS_INPUT));
        actor.attemptsTo(Enter.theValue(infoLaboral.getCargo()).into(CARGO_INPUT));
        actor.attemptsTo(Click.on(VICEPRESIDENCIA_COMBOBOX));
        actor.attemptsTo(Click.on(VICEPRESIDENCIA_LISTA(infoLaboral.getVicepresidenica())));
        actor.attemptsTo(Enter.theValue(infoLaboral.getAniosExperiencia()).into(ANIOS_EXPERIENCIA_INPUT));
        actor.attemptsTo(Click.on(GUARDAR_BUTTON));
        actor.attemptsTo(Wait.theSeconds(5));
    }

    // Este metodo nos llama el usuario desde el step
    public static DiligenciarInformacionHojaDeVidaES con(InformacionHojaDeVida infoLaboral) {
        return Tasks.instrumented(DiligenciarInformacionHojaDeVidaES.class, infoLaboral);
    }

}










