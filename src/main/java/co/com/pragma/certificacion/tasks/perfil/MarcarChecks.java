package co.com.pragma.certificacion.tasks.perfil;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.*;


public class MarcarChecks implements Task {


    // Marcar el check de si tiene visa
    @Override
    public <T extends Actor> void performAs(T actor) {
          actor.attemptsTo(Click.on(TENER_VISA_CHECK));
          actor.attemptsTo(Click.on(TRABAJAR_EEUU_CHECK));
          actor.attemptsTo(Click.on(GUARDAR_BUTTON));
    }


    // Este metodo nos llama el usuario desde el step
    public static MarcarChecks check() {
        return Tasks.instrumented(MarcarChecks.class);
    }

}










