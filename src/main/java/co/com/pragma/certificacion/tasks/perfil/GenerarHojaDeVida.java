package co.com.pragma.certificacion.tasks.perfil;

import co.com.pragma.certificacion.interations.DescargarHojaDeVida;
import co.com.pragma.certificacion.interations.SelecionarIdiomaHojaDeVida;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;


public class GenerarHojaDeVida implements Task {


    private String valor;

    public GenerarHojaDeVida(String valor) {
        this.valor = valor;
    }



        // Paso a paso de ingreso
    @Override
    public <T extends Actor> void performAs(T actor) {

     //Generar hoja de vida

        actor.attemptsTo(DescargarHojaDeVida.pagina());
        actor.attemptsTo(SelecionarIdiomaHojaDeVida.pagina(valor));

    }


    // Este metodo nos llama el usuario desde el step
    public static GenerarHojaDeVida con(String valor) {
        return Tasks.instrumented(GenerarHojaDeVida.class,valor);
    }

}










