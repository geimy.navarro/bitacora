package co.com.pragma.certificacion.tasks.pragmadata;

import co.com.pragma.certificacion.interations.Wait;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import static co.com.pragma.certificacion.userinterfaces.pragmadata.InicioPragmaData.*;



public class ExportarDatosPragmaData implements Task {

    private String opcionPragmaData;

    public ExportarDatosPragmaData(String opcionPragmaData) {
        this.opcionPragmaData = opcionPragmaData;
    }

    // selecciona el check de conocimientos tecnicos
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(PRAGMA_DATA_OPTION));
        actor.attemptsTo(Click.on(OPCION_PRAGMA_DATA_CHECKBOX(opcionPragmaData)));
        actor.attemptsTo(Click.on(EXPORTAR_BUTTON), Wait.theSeconds(10));
    }


    // Este metodo nos llama el usuario desde el step
    public static ExportarDatosPragmaData check(String opcionPragmaData) {
        return Tasks.instrumented(ExportarDatosPragmaData.class, opcionPragmaData);
    }

}

