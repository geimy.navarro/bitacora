package co.com.pragma.certificacion.tasks.perfil;

import co.com.pragma.certificacion.interations.Wait;
import co.com.pragma.certificacion.model.Experiencias;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.*;


public class DiliganciarExperienciasExternas implements Task {


    private Experiencias experiencias;

    public DiliganciarExperienciasExternas(Experiencias experiencias) {
        this.experiencias = experiencias;
    }

    // Paso a paso de ingreso
    @Override
    public <T extends Actor> void performAs(T actor) {

        // Se diligencia la informacion de experiencia laborar externa

        actor.attemptsTo(Click.on(AGREGAR_EXPERIENCIA_EXTERNA_BUTTON));
        actor.attemptsTo(Enter.theValue(experiencias.getProyecto()).into(EMPRESA_INPUT));
        System.out.println("VALIDE OME");
        actor.attemptsTo(Enter.theValue(experiencias.getRol()).into(ROL_INPUT));
        actor.attemptsTo(Enter.theValue(experiencias.getFechaIngreso()).into(FECHA_INGRESO_INPUT));
        actor.attemptsTo(Enter.theValue(experiencias.getFechaFinalizacion()).into(FECHA_FINALIZACION_INPUT));
        actor.attemptsTo(Enter.theValue(experiencias.getLogros()).into(LOGROS_INPUT));
        actor.attemptsTo(Enter.theValue(experiencias.getHerramientasUtilizadas()).
                into(HERRAMIENTAS_INPUT));
        actor.attemptsTo(Enter.theValue(experiencias.getDescripcion()).into(DESCRIPCION_INPUT));
        actor.attemptsTo(Click.on(GUARDAR_EXPERIENCIAS_BUTTON));
        actor.attemptsTo(Wait.theSeconds(5));
    }


    // Este metodo nos llama el usuario desde el step
    public static DiliganciarExperienciasExternas con(Experiencias experiencias) {
        return Tasks.instrumented(DiliganciarExperienciasExternas.class, experiencias);
    }

}










