package co.com.pragma.certificacion.tasks.perfil;


import co.com.pragma.certificacion.interations.Wait;
import co.com.pragma.certificacion.model.OtrosEstudios;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionAcademicaPage.*;
import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.*;


public class DiligenciarOtrosEstudios implements Task {

    private OtrosEstudios otrosEstudios;

    public DiligenciarOtrosEstudios(OtrosEstudios otrosEstudios) {
        this.otrosEstudios = otrosEstudios;
    }

    // Paso a paso de ingreso a informacion academica para otros estudios
    @Override
    public <T extends Actor> void performAs(T actor) {

        // Se diligencia la informacion academica  educacion de otros estudios

        actor.attemptsTo(Click.on(AGREGAR_OTROS_ESTUDIOS_BUTTON));
        actor.attemptsTo(Enter.theValue(otrosEstudios.getTituloCertificacion()).into(TITULO_DE_LA_CERTICACION_INPUT));
        actor.attemptsTo(Enter.theValue(otrosEstudios.getEmpresa()).into(EMPRESA_DICTA_INPUT));
        actor.attemptsTo(Enter.theValue(otrosEstudios.getAnioFinal()).into(ANIO_DE_FINALIZACION_OTROS_INPUT));
        actor.attemptsTo(Enter.theValue(otrosEstudios.getVigencia()).into(VIGENCIA_CERTIFICADO_INPUT));
        actor.attemptsTo(Enter.theValue(otrosEstudios.getDuracion()).into(DURACION_INPUT));
        actor.attemptsTo(Click.on(PALABRAS_CLAVES_COMBOBOX));
        actor.attemptsTo(Click.on(PALABRAS_CLAVES(otrosEstudios.getPalabrasClaves())), Wait.theSeconds(5));
        actor.attemptsTo(Click.on(GUARDAR_BUTTON), Wait.theSeconds(5));

    }
    // Este metodo nos llama el usuario desde el step
    public static DiligenciarOtrosEstudios con(OtrosEstudios otrosEstudios) {
        return Tasks.instrumented(DiligenciarOtrosEstudios.class, otrosEstudios);
    }

}










