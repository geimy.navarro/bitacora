package co.com.pragma.certificacion.tasks;

import co.com.pragma.certificacion.interations.Wait;
import co.com.pragma.certificacion.model.Usuario;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static co.com.pragma.certificacion.userinterfaces.InicioSesionPage.*;
import static co.com.pragma.certificacion.utils.LeerCodigos.getTwoFactorCode;

public class IniciarSesion implements Task {

    private Usuario usuario;

    public IniciarSesion(Usuario usuario) {
        this.usuario = usuario;
    }

    // Paso a paso de inicio de sesion
    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(INICIAR_SESION_CON_GOOGLE_BOTTON));
        actor.attemptsTo(Enter.theValue(usuario.getCorreo()).into(CORREO_INPUT));
        actor.attemptsTo(Click.on(SIGUIENTE_BUTTON), Wait.theSeconds(1));
        actor.attemptsTo(Enter.theValue(usuario.getContrasenia()).into(CONTRASENIA_INPUT));
        actor.attemptsTo(Click.on(SIGUIENTE_BUTTON), Wait.theSeconds(1));
        actor.attemptsTo(Click.on(PROBAR_OTRA_MANERA_BUTTON));
        actor.attemptsTo(Click.on(USAR_AUTEN_BUTTON));
        actor.attemptsTo(Enter.theValue(getTwoFactorCode()).into(OPT_INPUT));
        actor.attemptsTo(Click.on(SIGUIENTE_BUTTON));
        actor.attemptsTo(Wait.theSeconds(15));

    }

    // Este metodo nos llama el usuario desde el step
    public static IniciarSesion con(Usuario usuario) {

        return Tasks.instrumented(IniciarSesion.class, usuario);
    }

}










