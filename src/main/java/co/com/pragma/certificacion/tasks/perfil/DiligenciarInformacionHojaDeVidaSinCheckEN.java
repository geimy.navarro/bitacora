package co.com.pragma.certificacion.tasks.perfil;

import co.com.pragma.certificacion.model.InformacionHojaDeVida;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.*;


public class DiligenciarInformacionHojaDeVidaSinCheckEN implements Task {

    private InformacionHojaDeVida infoLaboral;
    public DiligenciarInformacionHojaDeVidaSinCheckEN(InformacionHojaDeVida infoLaboral) {
        this.infoLaboral = infoLaboral;
    }

    // Paso a paso de ingreso para cambiar el idioma
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SWITCH_BUTTON));
        actor.attemptsTo(Enter.theValue(infoLaboral.getProfesion()).into(PROFESION_COMBOBOX));
        actor.attemptsTo(Enter.theValue(infoLaboral.getPrefilProfesional()).into(PERFIL_PROFESIONAL_INPUT));
        actor.attemptsTo(Enter.theValue(infoLaboral.getConocimientosTecnicos()).into(CONOCIMIENTOS_TECNICOS_INPUT));
        actor.attemptsTo(Scroll.to(PERFIL_PROFESIONAL_INPUT).andAlignToTop());
        actor.attemptsTo(Enter.theValue(infoLaboral.getCargo()).into(CARGO_INPUT));
        actor.attemptsTo(Click.on(VICEPRESIDENCIA_COMBOBOX));
        actor.attemptsTo(Click.on(VICEPRESIDENCIA_LISTA(infoLaboral.getVicepresidenica())));
        actor.attemptsTo(Enter.theValue(infoLaboral.getAniosExperiencia()).into(ANIOS_EXPERIENCIA_INPUT));
        actor.attemptsTo(Click.on(GUARDAR_BUTTON));

    }


    // Este metodo nos llama el usuario desde el step
    public static DiligenciarInformacionHojaDeVidaSinCheckEN con (InformacionHojaDeVida infoLaboral) {
        return Tasks.instrumented(DiligenciarInformacionHojaDeVidaSinCheckEN.class, infoLaboral);
    }

}










