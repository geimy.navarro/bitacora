package co.com.pragma.certificacion.tasks.perfil;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.*;


public class MarcarCheckTrabajarE implements Task {


    // Marca el check de que quiere trabajar en EEUU
    @Override
    public <T extends Actor> void performAs(T actor) {
          actor.attemptsTo(Click.on(TRABAJAR_EEUU_CHECK));
          actor.attemptsTo(Click.on(GUARDAR_BUTTON));


    }


    // Este metodo nos llama el usuario desde el step
    public static MarcarCheckTrabajarE check() {
        return Tasks.instrumented(MarcarCheckTrabajarE.class);
    }

}










