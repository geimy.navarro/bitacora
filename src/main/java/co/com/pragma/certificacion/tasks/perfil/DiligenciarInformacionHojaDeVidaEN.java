package co.com.pragma.certificacion.tasks.perfil;

import co.com.pragma.certificacion.model.InformacionHojaDeVida;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.*;


public class DiligenciarInformacionHojaDeVidaEN implements Task {

    private InformacionHojaDeVida infoLaboral;
    public DiligenciarInformacionHojaDeVidaEN(InformacionHojaDeVida infoLaboral) {
        this.infoLaboral = infoLaboral;
    }

    // Paso a paso de ingreso
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(infoLaboral.getProfesion()).into(PROFESION_COMBOBOX));
        actor.attemptsTo(Enter.theValue(infoLaboral.getPrefilProfesional()).into(PERFIL_PROFESIONAL_INPUT));
        actor.attemptsTo(Enter.theValue(infoLaboral.getConocimientosTecnicos()).into(CONOCIMIENTOS_TECNICOS_INPUT));
        actor.attemptsTo(Scroll.to(PERFIL_PROFESIONAL_INPUT).andAlignToTop());
        actor.attemptsTo(Enter.theValue(infoLaboral.getCargo()).into(CARGO_INPUT));
        actor.attemptsTo(Click.on(VICEPRESIDENCIA_COMBOBOX));
        actor.attemptsTo(Click.on(VICEPRESIDENCIA_LISTA(infoLaboral.getVicepresidenica())));
        actor.attemptsTo(Enter.theValue(infoLaboral.getAniosExperiencia()).into(ANIOS_EXPERIENCIA_INPUT));

    }


    // Este metodo nos llama el usuario desde el step
    public static DiligenciarInformacionHojaDeVidaEN con(InformacionHojaDeVida infoLaboral) {
        return Tasks.instrumented(DiligenciarInformacionHojaDeVidaEN.class, infoLaboral);
    }

}










