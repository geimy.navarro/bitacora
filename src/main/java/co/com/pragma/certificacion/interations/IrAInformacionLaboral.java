package co.com.pragma.certificacion.interations;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;


import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionPerfilPage.INFORMACION_LABORAL_TAB;


public class IrAInformacionLaboral implements Interaction {

    //hace clic y se dirige a informacion laboral
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(ScrollTop.pagina());
        actor.attemptsTo(Click.on(INFORMACION_LABORAL_TAB),Wait.theSeconds(5));
    }

    public static IrAInformacionLaboral pagina() {
        return new IrAInformacionLaboral();

    }
}