package co.com.pragma.certificacion.interations;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SelectFromOptions;


import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionPerfilPage.*;


public class SelecionarIdiomaHojaDeVida implements Interaction {

    private String valor;

    // selecciona el idioma en el cual se generar la hoja de vida

    public SelecionarIdiomaHojaDeVida(String valor) {
        this.valor = valor;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(SelectFromOptions.byValue(valor).from(IDIOMA_HOJA_DE_VIDA_BUTTON));
        actor.attemptsTo(Click.on(GENERAR_HOJA_DE_VIDA_BUTTON), Wait.theSeconds(10));

    }

    public static SelecionarIdiomaHojaDeVida pagina(String valor) {
        return new SelecionarIdiomaHojaDeVida(valor);

    }
}