package co.com.pragma.certificacion.interations;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import static co.com.pragma.certificacion.userinterfaces.InicioPage.EDITAR_PERFIL_OPTION;
import static co.com.pragma.certificacion.userinterfaces.InicioPage.FOTO_PERFIL_DIV;


public class IrAEditarPerfil implements Interaction {


    // Hace clic en el encabezado de editar perfil
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(FOTO_PERFIL_DIV),Wait.theSeconds(5));
        actor.attemptsTo(Click.on(EDITAR_PERFIL_OPTION), Wait.theSeconds(5));
    }

    public static IrAEditarPerfil pagina() {
        return new IrAEditarPerfil();

    }
}