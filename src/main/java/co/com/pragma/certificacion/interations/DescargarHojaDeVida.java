package co.com.pragma.certificacion.interations;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionPerfilPage.DESCARGAR_HOJA_DE_VIDA_BUTTON;




public class DescargarHojaDeVida implements Interaction {


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(DESCARGAR_HOJA_DE_VIDA_BUTTON),Wait.theSeconds(5));

    }

    public static DescargarHojaDeVida pagina() {
        return new DescargarHojaDeVida();

    }
}