package co.com.pragma.certificacion.interations;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionLaboralPage.SWITCH_BUTTON;



public class CambiarIdioma implements Interaction {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(SWITCH_BUTTON),Wait.theSeconds(5));

    }

    public static CambiarIdioma pagina() {
        return new CambiarIdioma();

    }
}