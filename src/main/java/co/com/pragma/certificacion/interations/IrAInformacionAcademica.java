package co.com.pragma.certificacion.interations;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;


import static co.com.pragma.certificacion.userinterfaces.perfil.InformacionPerfilPage.INFORMACION_ACADEMICA_TAB;


public class IrAInformacionAcademica implements Interaction {

    // hace clic y se dirige a informacion academica
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(ScrollTop.pagina());
        actor.attemptsTo(Click.on(INFORMACION_ACADEMICA_TAB),Wait.theSeconds(5));
    }

    public static IrAInformacionAcademica pagina() {
        return new IrAInformacionAcademica();

    }
}