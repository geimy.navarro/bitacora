package co.com.pragma.certificacion.interations;

import net.serenitybdd.screenplay.Ability;
import net.serenitybdd.screenplay.Actor;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;


//leer el excel con los datos
public class LeerHojaDeCalculo implements Ability {

    private String rutaArchivo;
    private String nombreHoja;
    private static XSSFSheet xssfSheet;

    public LeerHojaDeCalculo(String rutaArchivo, String nombreHoja) {
        this.nombreHoja = nombreHoja;
        this.rutaArchivo = rutaArchivo;
        actulizarArchivo(rutaArchivo, nombreHoja);
    }

    public static LeerHojaDeCalculo as(Actor actor) {
        return actor.abilityTo(LeerHojaDeCalculo.class);
    }

    public static LeerHojaDeCalculo by(String rutaArchivo, String nombreHoja) {
        return new LeerHojaDeCalculo(rutaArchivo, nombreHoja);
    }

    public void actulizarArchivo(String rutaArchivo, String nombreHoja) {
        try {
            FileInputStream fileInputStream = new FileInputStream(rutaArchivo);
            XSSFWorkbook xssfWorkbook = new XSSFWorkbook(fileInputStream);
            xssfSheet = xssfWorkbook.getSheet(nombreHoja);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String obtenerInformacion(int numFila, int numColumna) {
        XSSFRow xssfRow = xssfSheet.getRow(numFila);
        XSSFCell xssfCell = xssfRow.getCell(numColumna);
        return xssfCell.getStringCellValue();
    }


}
