package co.com.pragma.certificacion.model;

public class OtrosEstudios {
 private String tituloCertificacion;
 private String empresa;
 private String anioFinal;
 private String vigencia;
 private String duracion;
 private String palabrasClaves;

    public OtrosEstudios(String tituloCertificacion, String empresa, String anioFinal,
                         String vigencia, String duracion, String palabrasClaves) {
        this.tituloCertificacion = tituloCertificacion;
        this.empresa = empresa;
        this.anioFinal = anioFinal;
        this.vigencia = vigencia;
        this.duracion = duracion;
        this.palabrasClaves = palabrasClaves;
    }

    public String getTituloCertificacion() {
        return tituloCertificacion;
    }

    public String getEmpresa() {
        return empresa;
    }

    public String getAnioFinal() {
        return anioFinal;
    }

    public String getVigencia() {
        return vigencia;
    }

    public String getDuracion() {
        return duracion;
    }

    public String getPalabrasClaves() {
        return palabrasClaves;
    }
}
