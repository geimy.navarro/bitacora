package co.com.pragma.certificacion.model;

public class InformacionHojaDeVida {

    private String profesion;
    private String prefilProfesional;
    private String conocimientosTecnicos;
    private  String cargo;
    private String vicepresidenica;
    private  String aniosExperiencia;


    public InformacionHojaDeVida(String profesion, String prefilProfesional, String conocimientosTecnicos,
                                 String cargo, String vicepresidenica, String aniosExperiencia) {
        this.profesion = profesion;
        this.prefilProfesional = prefilProfesional;
        this.conocimientosTecnicos = conocimientosTecnicos;
        this.cargo = cargo;
        this.vicepresidenica = vicepresidenica;
        this.aniosExperiencia = aniosExperiencia;
    }

    public String getProfesion() {
        return profesion;
    }

    public String getPrefilProfesional() {
        return prefilProfesional;
    }

    public String getConocimientosTecnicos() {
        return conocimientosTecnicos;
    }

    public String getCargo() {
        return cargo;
    }

    public String getVicepresidenica() {
        return vicepresidenica;
    }

    public String getAniosExperiencia() {
        return aniosExperiencia;
    }
}
