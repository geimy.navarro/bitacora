package co.com.pragma.certificacion.model;

public class Educacion {
    private String tipoNivel;
    private String tituloObtenido;
    private String anioFinalizacion;
    private String institucion;

    public Educacion(String tipoNivel, String tituloObtenido, String anioFinalizacion, String institucion) {
        this.tipoNivel = tipoNivel;
        this.tituloObtenido = tituloObtenido;
        this.anioFinalizacion = anioFinalizacion;
        this.institucion = institucion;
    }

    public String getTipoNivel() {
        return tipoNivel;
    }

    public String getTituloObtenido() {
        return tituloObtenido;
    }

    public String getAnioFinalizacion() {
        return anioFinalizacion;
    }

    public String getInstitucion() {
        return institucion;
    }
}
