package co.com.pragma.certificacion.model;

public class Experiencias {

    private String proyecto;
    private String rol;
    private String fechaIngreso;
    private String fechaFinalizacion;
    private String logros;
    private String herramientasUtilizadas;
    private String descripcion;

    public Experiencias(String proyecto, String rol, String fechaIngreso, String fechaFinalizacion,
                        String logros, String herramientasUtilizadas, String descripcion) {
        this.proyecto = proyecto;
        this.rol = rol;
        this.fechaIngreso = fechaIngreso;
        this.fechaFinalizacion = fechaFinalizacion;
        this.logros = logros;
        this.herramientasUtilizadas = herramientasUtilizadas;
        this.descripcion = descripcion;
    }

    public String getProyecto() {
        return proyecto;
    }

    public String getRol() {
        return rol;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public String getFechaFinalizacion() {
        return fechaFinalizacion;
    }

    public String getLogros() {
        return logros;
    }

    public String getHerramientasUtilizadas() {
        return herramientasUtilizadas;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
