package co.com.pragma.certificacion.model;

import java.util.List;
import java.util.Map;

public class Usuario {
    private String correo;
    private String contrasenia;

    public Usuario(List<Map<String, String>> datos) {
        this.correo = datos.get(0).get("correo");
        this.contrasenia = datos.get(0).get("contrasenia");
    }

    public String getCorreo() {
        return correo;
    }

    public String getContrasenia() {
        return contrasenia;
    }
}