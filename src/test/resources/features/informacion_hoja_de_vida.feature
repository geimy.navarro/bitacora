# language: es


  Característica: Informacion laboral

    Antecedentes: Iniciar sesion

      Dado que Geimy Navarro esta en la pagina de inicio
        | correo                         | contrasenia|
        | geimy.navarro@pragma.com.co    | G5313203* |



    Escenario: Diligenciar Informacion laboral  marcando los checks en español
      Cuando  diligencie el formulario informacion hoja de vida  marcando los checks en espanol
      Entonces deberia visualizar la alerta con el texto Se ha guardado exitosamente


    Escenario: Diligenciar Informacion laboral sin marcar los checks en español
      Cuando  diligencie el formulario informacion hoja de vida sin marcar los checks en espanol
      Entonces deberia visualizar la alerta con el texto Se ha guardado exitosamente


    Escenario: Diligenciar Informacion laboral marcando el check de visa en espanol
      Cuando  diligencie el formulario informacion hoja de vida marcando el check de visa en espanol
      Entonces deberia visualizar la alerta con el texto Se ha guardado exitosamente


    Escenario: Diligenciar Informacion laboral marcando el check de trabajar en EEUU en espanol
      Cuando  diligencie el formulario informacion hoja de vida marcando el check de trabajar en EEUU en espanol
      Entonces deberia visualizar la alerta con el texto Se ha guardado exitosamente

    @Tag
    Escenario: Diligenciar Informacion laboral  marcando los checks sin guardar  los cambios en espanol
      Cuando  diligencie el formulario informacion hoja de vida marcando los checks sin guardar los cambios en espanol
      Entonces deberia estar deschequeado


      #escenarios ingles

    Escenario: Diligenciar Informacion laboral  marcando los checks en ingles
      Cuando  diligencie el formulario informacion hoja de vida  marcando los checks en ingles
      Entonces deberia visualizar la alerta con el texto Se ha guardado exitosamente


    Escenario: Diligenciar Informacion laboral sin marcar los checks en ingles
      Cuando  diligencie el formulario informacion hoja de vida sin marcar los checks en ingles
      Entonces deberia visualizar la alerta con el texto Se ha guardado exitosamente


    Escenario: Diligenciar Informacion laboral marcando el check de visa en ingles
      Cuando  diligencie el formulario informacion hoja de vida marcando el check de visa en ingles
      Entonces deberia visualizar la alerta con el texto Se ha guardado exitosamente


    Escenario: Diligenciar Informacion laboral marcando el check de trabajar en EEUU en ingles
      Cuando  diligencie el formulario informacion hoja de vida marcando el check de trabajar en EEUU en ingles
      Entonces deberia visualizar la alerta con el texto Se ha guardado exitosamente


    Escenario: Diligenciar Informacion laboral  marcando los checks sin guardar  los cambios en ingles
      Cuando  diligencie el formulario informacion hoja de vida marcando los checks sin guardar los cambios en ingles
      Entonces deberia estar deschequeado