# language: es


  Característica: Iniciar Sesión


    Escenario: el usuario ingresa exitosamente al sitio web Bitacora
      Dado que Geimy accede al sitio web de Bitacora
      Cuando ingrese con sus credenciales

        | correo                         | contrasenia|
        | apps-ecosistemas@pragma.com.co | Pruebas22* |

      Entonces ver la pagina de inicio de bitacora

