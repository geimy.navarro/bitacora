# language: es


  Característica: Informacion laboral

    Antecedentes: Iniciar sesion
      Dado que Geimy Navarro esta en la pagina de inicio

        | correo                         | contrasenia|
        | geimy.navarro@pragma.com.co | G5313203* |



    Escenario: Generar hoja de vida en espanol
      Dado que diligencio la informacion de la hoja de vida en espanol
      Cuando  quiera generar su hoja de vida en espanol
      Entonces deberia visualizar la descarga

      @ingles
    Escenario: Generar hoja de vida en ingles
      Dado que diligencio la informacion de la hoja de vida en ingles
      Cuando  quiera generar su hoja de vida en ingles
      Entonces deberia visualizar la descarga