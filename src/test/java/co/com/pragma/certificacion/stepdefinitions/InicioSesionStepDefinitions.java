package co.com.pragma.certificacion.stepdefinitions;


import co.com.pragma.certificacion.model.Usuario;
import co.com.pragma.certificacion.questions.LaPaginaDeInicio;
import co.com.pragma.certificacion.tasks.IniciarSesion;
import co.com.pragma.certificacion.utils.MyDriver;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.hamcrest.core.IsEqual;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;


public class InicioSesionStepDefinitions {


    @Dado("que {actor} accede al sitio web de Bitacora")
    public void queUsuarioAccedeAlSitioWebDeBitacora(Actor actor) {
        //  theActorInTheSpotlight().whoCan(BrowseTheWeb.with(MyDriver.aChromeDriver().inTheWebPage("https://d3jvm6ee97l28q.cloudfront.net/login")));
        actor.whoCan(BrowseTheWeb.with(MyDriver.aFirefox().inTheWebPage("https://bitacora.pragma.com.co/login")));

    }


    @Cuando("ingrese con sus credenciales")
    public void elUsuarioIngreseConSusCredenciales(List<Map<String, String>> credenciales) {
        Usuario usu = new Usuario(credenciales);
        theActorInTheSpotlight().attemptsTo(IniciarSesion.con(usu));
    }


    @Entonces("ver la pagina de inicio de bitacora")
    public void verLaPaginaDeInicioDeBitacora() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(LaPaginaDeInicio.visible(), IsEqual.equalTo(true)));
    }

    @Dado("que {actor} esta en la pagina de inicio")
    public void queUsuarioEstaEnLaPaginaDeInicio(Actor actor, List<Map<String, String>> credenciales) {
        //theActorInTheSpotlight().whoCan(BrowseTheWeb.with(MyDriver.aChromeDriver().inTheWebPage("https://d3jvm6ee97l28q.cloudfront.net/login")));
        actor.whoCan(BrowseTheWeb.with(MyDriver.aFirefox().inTheWebPage("https://bitacora.pragma.com.co/login")));
        Usuario usu = new Usuario(credenciales);
        theActorInTheSpotlight().wasAbleTo(IniciarSesion.con(usu));
    }
}


















