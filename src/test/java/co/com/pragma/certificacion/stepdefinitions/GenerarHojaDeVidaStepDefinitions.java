package co.com.pragma.certificacion.stepdefinitions;


import co.com.pragma.certificacion.interations.*;
import co.com.pragma.certificacion.model.Educacion;
import co.com.pragma.certificacion.model.Experiencias;
import co.com.pragma.certificacion.model.InformacionHojaDeVida;
import co.com.pragma.certificacion.model.OtrosEstudios;
import co.com.pragma.certificacion.questions.LaDescarga;

import co.com.pragma.certificacion.tasks.perfil.*;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import org.hamcrest.core.IsEqual;


import static co.com.pragma.certificacion.interations.LeerHojaDeCalculo.obtenerInformacion;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;


public class GenerarHojaDeVidaStepDefinitions {

//step en español

    private static final String RUTA_ARCHIVO = "src/test/resources/datos/datos.xlsx";
    private static final String NOMBRE_HOJA = "laboral";


    @Dado("que diligencio la informacion de la hoja de vida en espanol")
    public void queDiligencioLaInformacionDeLaHojaDeVidaEnEspanol() {
        int numFila = 2;
        theActorInTheSpotlight().wasAbleTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().wasAbleTo(IrAInformacionLaboral.pagina());

        theActorInTheSpotlight().can(LeerHojaDeCalculo.by(RUTA_ARCHIVO, "laboral"));
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(obtenerInformacion(numFila, 1),
                obtenerInformacion(numFila, 3), obtenerInformacion(numFila, 4),
                obtenerInformacion(numFila, 5), obtenerInformacion(numFila, 6),
                obtenerInformacion(numFila, 7));

        theActorInTheSpotlight().wasAbleTo(DiligenciarInformacionHojaDeVidaES.con(infoLaboral));
        Experiencias experienciaPragma = new Experiencias(obtenerInformacion(numFila, 11),
                obtenerInformacion(numFila, 12), obtenerInformacion(numFila, 15),
                obtenerInformacion(numFila, 16), obtenerInformacion(numFila, 17),
                obtenerInformacion(numFila, 18), obtenerInformacion(numFila, 19));
        theActorInTheSpotlight().wasAbleTo(DiliganciarExperienciasPragma.con(experienciaPragma));

        Experiencias experienciaExterna = new Experiencias(obtenerInformacion(numFila, 20),
                obtenerInformacion(numFila, 21), obtenerInformacion(numFila, 22),
                obtenerInformacion(numFila, 23), obtenerInformacion(numFila, 24),
                obtenerInformacion(numFila, 25), obtenerInformacion(numFila, 26));
        theActorInTheSpotlight().wasAbleTo(DiliganciarExperienciasExternas.con(experienciaExterna));



        //Diligenciar estudios
        theActorInTheSpotlight().wasAbleTo(IrAInformacionAcademica.pagina());

        theActorInTheSpotlight().can(LeerHojaDeCalculo.by(RUTA_ARCHIVO, "academica"));
        Educacion educacion = new Educacion(obtenerInformacion(numFila, 1),
                obtenerInformacion(numFila, 2), obtenerInformacion(numFila, 3),
                obtenerInformacion(numFila, 4));
        theActorInTheSpotlight().wasAbleTo(DiligenciarEducacion.con(educacion));



        theActorInTheSpotlight().wasAbleTo(IrAInformacionAcademica.pagina());
        theActorInTheSpotlight().can(LeerHojaDeCalculo.by(RUTA_ARCHIVO, "academica"));
        OtrosEstudios otrosEstudios = new OtrosEstudios(obtenerInformacion(numFila, 5),
                obtenerInformacion(numFila, 6), obtenerInformacion(numFila, 7),
                obtenerInformacion(numFila, 8), obtenerInformacion(numFila, 9),
                obtenerInformacion(numFila, 10));
        theActorInTheSpotlight().wasAbleTo(DiligenciarOtrosEstudios.con(otrosEstudios));


    }
//Diligenciar la hoja de vida en inlges
    @Dado("que diligencio la informacion de la hoja de vida en ingles")
    public void queDiligencioLaInformacionDeLaHojaDeVidaEnIngles() {
        int numFila = 3;
        //interacciones
        theActorInTheSpotlight().wasAbleTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().wasAbleTo(IrAInformacionLaboral.pagina());
        theActorInTheSpotlight().wasAbleTo(CambiarIdioma.pagina());

        theActorInTheSpotlight().can(LeerHojaDeCalculo.by(RUTA_ARCHIVO, "laboral"));
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(obtenerInformacion(numFila, 1),
                obtenerInformacion(numFila, 3), obtenerInformacion(numFila, 4),
                obtenerInformacion(numFila, 5), obtenerInformacion(numFila, 6),
                obtenerInformacion(numFila, 7));

        theActorInTheSpotlight().wasAbleTo(DiligenciarInformacionHojaDeVidaEN.con(infoLaboral));
        Experiencias experienciaPragma = new Experiencias(obtenerInformacion(numFila, 11),
                obtenerInformacion(numFila, 12), obtenerInformacion(numFila, 13),
                obtenerInformacion(numFila, 14), obtenerInformacion(numFila, 15),
                obtenerInformacion(numFila, 16), obtenerInformacion(numFila, 17));
        theActorInTheSpotlight().wasAbleTo(DiliganciarExperienciasPragma.con(experienciaPragma));

        Experiencias experienciaExterna = new Experiencias(obtenerInformacion(numFila, 18),
                obtenerInformacion(numFila, 19), obtenerInformacion(numFila, 20),
                obtenerInformacion(numFila, 21), obtenerInformacion(numFila, 22),
                obtenerInformacion(numFila, 23), obtenerInformacion(numFila, 24));
        theActorInTheSpotlight().wasAbleTo(DiliganciarExperienciasExternas.con(experienciaExterna));



        //Diligenciar estudios
        //interacciones
        theActorInTheSpotlight().wasAbleTo(IrAInformacionAcademica.pagina());

        theActorInTheSpotlight().can(LeerHojaDeCalculo.by(RUTA_ARCHIVO, "academica"));
        Educacion educacion = new Educacion(obtenerInformacion(numFila, 1),
                obtenerInformacion(numFila, 2), obtenerInformacion(numFila, 3),
                obtenerInformacion(numFila, 4));
        theActorInTheSpotlight().wasAbleTo(DiligenciarEducacion.con(educacion));



        theActorInTheSpotlight().wasAbleTo(IrAInformacionAcademica.pagina());
        theActorInTheSpotlight().can(LeerHojaDeCalculo.by(RUTA_ARCHIVO, "academica"));
        OtrosEstudios otrosEstudios = new OtrosEstudios(obtenerInformacion(numFila, 5),
                obtenerInformacion(numFila, 6), obtenerInformacion(numFila, 7),
                obtenerInformacion(numFila, 8), obtenerInformacion(numFila, 9),
                obtenerInformacion(numFila, 10));
        theActorInTheSpotlight().wasAbleTo(DiligenciarOtrosEstudios.con(otrosEstudios));


    }
    @Cuando("quiera generar su hoja de vida en espanol")
    public void quieraGenerarSuHojaDeVidaEnEspanol() {
        theActorInTheSpotlight().wasAbleTo(GenerarHojaDeVida.con("2"));
    }

    @Cuando("quiera generar su hoja de vida en ingles")
    public void quieraGenerarSuHojaDeVidaEnIngles() {
        theActorInTheSpotlight().wasAbleTo(GenerarHojaDeVida.con("1"));
    }

        @Entonces("deberia visualizar la descarga")
    public void deberiaVisualizarLaDescarga() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(LaDescarga.esExitosa(
                "C:/Users/geimy.navarro_pragma/Downloads/HV_GEIMY_MARCELA_NAVARRO_MONSALVE.pdf"),
                IsEqual.equalTo(true)));

    }
}