package co.com.pragma.certificacion.stepdefinitions;

import co.com.pragma.certificacion.questions.LaDescarga;
import co.com.pragma.certificacion.tasks.pragmadata.ExportarDatosPragmaData;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import org.hamcrest.core.IsEqual;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;


public class ExportarDatosPragmaDataStepDefinitions {

    @Dado("que el {actor} se encuentre en la seccion de pragmadata")
    public void queElUsuarioSeEncuentreEnLaSeccionDePragmadata(Actor actor) {
        //     actor.wasAbleTo(OpcionesDeInicio.irPragmaData());

    }

    @Cuando("exporta la data de conocimientos tecnicos")
    public void exportaLaDataDeConocimientosTecnicos() {
        OnStage.theActorInTheSpotlight().attemptsTo(ExportarDatosPragmaData.check("45"));

    }

    @Entonces("deberia visualizar el archivo")
    public void deberiaVisualizarElArchivo() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(LaDescarga.esExitosa(
                        "C:/Users/geimy.navarro_pragma/Downloads/PragmaData-2022-08-23T01_01_17.904Z.xlsx"),
                IsEqual.equalTo(true)));

    }
}
