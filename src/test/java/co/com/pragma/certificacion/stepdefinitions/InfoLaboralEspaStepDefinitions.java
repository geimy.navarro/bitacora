package co.com.pragma.certificacion.stepdefinitions;


import co.com.pragma.certificacion.interations.IrAEditarPerfil;
import co.com.pragma.certificacion.interations.IrAInformacionLaboral;
import co.com.pragma.certificacion.model.InformacionHojaDeVida;
import co.com.pragma.certificacion.model.Usuario;
import co.com.pragma.certificacion.questions.ElMensajeDeLaAlerta;
import co.com.pragma.certificacion.questions.EstaChekeado;
import co.com.pragma.certificacion.tasks.IniciarSesion;
import co.com.pragma.certificacion.tasks.perfil.*;
import co.com.pragma.certificacion.utils.MyDriver;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;


import java.util.List;
import java.util.Map;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.core.IsEqual.equalTo;


public class InfoLaboralEspaStepDefinitions {

//step en español



    @Cuando("diligencie el formulario informacion hoja de vida  marcando los checks en espanol")
    public void diligencieElFormularioInformacionHojadeVidaMarcandoLosChecksEnEspanol(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaES.con(infoLaboral),
                MarcarChecks.check());

    }

    @Cuando("diligencie el formulario informacion hoja de vida sin marcar los checks en espanol")
    public void diligencieElFormularioInformacionHojadeVidaSinMarcarLosChecksEnEspanol(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaSinCheckES.con(infoLaboral));

    }

    @Cuando("diligencie el formulario informacion hoja de vida marcando el check de visa en espanol")
    public void diligencieElFormularioInformacionHojadeVidaMarcandorElCheckDeVisaEnEspanol(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaES.con(infoLaboral),
                MarcarCheckVisa.check());

    }

    @Cuando("diligencie el formulario informacion hoja de vida marcando el check de trabajar en EEUU en espanol")
    public void diligencieElFormularioInformacionHojadeVidaSinMarcandoElCheckDeTrabajarEnEEUUEnEspanol(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaES.con(infoLaboral),
                MarcarCheckTrabajarE.check());
    }

    @Cuando("diligencie el formulario informacion hoja de vida marcando los checks sin guardar los cambios en espanol")
    public void diligencieElFormularioInformacionHojadeVidaMarcandoLosChecksSinGuardarLosCambiosEnEspanol(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaES.con(infoLaboral),
                MarcarChecksSinGuardar.check(), RefrescarPagina.delNavegador());
    }
//step ingles

    @Cuando("diligencie el formulario informacion hoja de vida  marcando los checks en ingles")
    public void diligencieElFormularioInformacionHojadeVidaMarcandoLosChecksEnIngles(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaEN.con(infoLaboral),
                MarcarChecks.check());
    }

    @Cuando("diligencie el formulario informacion hoja de vida sin marcar los checks en ingles")
    public void diligencieElFormularioInformacionHojadeVidaSinMarcarLosChecksEnIngles(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaSinCheckEN.con(infoLaboral));

    }

    @Cuando("diligencie el formulario informacion hoja de vida marcando el check de visa en ingles")
    public void diligencieElFormularioInformacionHojadeVidaMarcandorElCheckDeVisaEnIngles(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaEN.con(infoLaboral),
                MarcarCheckVisa.check());

    }

    @Cuando("diligencie el formulario informacion hoja de vida marcando el check de trabajar en EEUU en ingles")
    public void diligencieElFormularioInformacionHojadeVidaSinMarcandoElCheckDeTrabajarEnEEUUEnIngles(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaEN.con(infoLaboral),
                MarcarCheckTrabajarE.check());
    }

    @Cuando("diligencie el formulario informacion hoja de vida marcando los checks sin guardar los cambios en ingles")
    public void diligencieElFormularioInformacionHojadeVidaMarcandoLosChecksSinGuardarLosCambiosEnIngles(DataTable dataTable) {
        int numFila = 1;
        List<List<String>> datos = dataTable.asLists(String.class);
        InformacionHojaDeVida infoLaboral = new InformacionHojaDeVida(datos.get(numFila).get(0), datos.get(numFila).get(1),
                datos.get(numFila).get(2), datos.get(numFila).get(3), datos.get(numFila).get(4), datos.get(numFila).get(5));
        theActorInTheSpotlight().attemptsTo(IrAEditarPerfil.pagina());
        theActorInTheSpotlight().attemptsTo(IrAInformacionLaboral.pagina(),
                DiligenciarInformacionHojaDeVidaEN.con(infoLaboral),
                MarcarChecksSinGuardar.check(), RefrescarPagina.delNavegador());

    }


    @Entonces("deberia estar deschequeado")
    public void deberiaEstarDeschequeado() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(EstaChekeado.visible(),
                equalTo(true)));
    }

    @Entonces("deberia visualizar la alerta con el texto (.*)$")
    public void deberiaVisualizarLaAlertaConElTexto(String alerta) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ElMensajeDeLaAlerta.text(), equalTo(alerta)));
    }

}

